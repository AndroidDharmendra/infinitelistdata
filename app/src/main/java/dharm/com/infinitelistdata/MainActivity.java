package dharm.com.infinitelistdata;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dharm.com.infinitelistdata.adater.CustomListAdapter;
import dharm.com.infinitelistdata.app.AppController;
import dharm.com.infinitelistdata.model.DataModel;
import dharm.com.infinitelistdata.util.ConnectionDetector;

public class MainActivity extends AppCompatActivity implements AbsListView.OnScrollListener {
    private static final String url = "http://www.reddit.com/r/pics/hot.json";
    private ProgressDialog pDialog;

    private static final String TAG = MainActivity.class.getSimpleName();
    private List<DataModel> datalist = new ArrayList<DataModel>();
    private ListView listView;
    private String toMore = "";
    private ProgressBar progressBar;
    private CustomListAdapter adapter;
    private int currentScrollState;
    private int currentVisibleItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());


        if (cd.isConnectingToInternet() == true) {
            pDialog.setMessage("Loading...");
            pDialog.show();
            getJsonData(url);
        } else {
            Toast.makeText(MainActivity.this, "Please check your network connection & try again later!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void init() {
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnScrollListener(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        adapter = new CustomListAdapter(MainActivity.this, datalist);
        listView.setAdapter(adapter);
    }

    public void getJsonData(String url) {
        String requestted_url = url;
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, requestted_url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        hidePDialog();
                        //Getting responce and parsing it----
                        try {
                            String data = response.getString("data");
                            JSONObject dataobj = new JSONObject(data);
                            toMore = dataobj.getString("after");
                            JSONArray children_array = dataobj.getJSONArray("children");
                            for (int i = 0; i < children_array.length(); i++) {
                                JSONObject jobj = children_array.getJSONObject(i);
                                if (jobj.has("data")) {
                                    String data1 = jobj.getString("data");
                                    JSONObject data1obj = new JSONObject(data1);
                                    String thumbnail = data1obj.getString("thumbnail");
                                    String title = data1obj.getString("title");
                                    String permalink = data1obj.getString("permalink");
                                    if (data1obj.has("preview") && (!thumbnail.equals("self"))) {
                                        JSONObject prevjobj = new JSONObject(data1obj.getString("preview"));
                                        JSONArray prevArray = prevjobj.getJSONArray("images");

                                        JSONObject imgobj = prevArray.getJSONObject(0);
                                        String sourcrdata = imgobj.getString("source");
                                        JSONObject sourceobj = new JSONObject(sourcrdata);
                                        String url_large = sourceobj.getString("url");
                                        DataModel dtmdl = new DataModel();
                                        dtmdl.setLargeImage_url(url_large);
                                        dtmdl.setThumbnailUrl(thumbnail);
                                        dtmdl.setTitle(title);
                                        dtmdl.setPermalinkstr(permalink);
                                        datalist.add(dtmdl);
                                    }
                                }
                            }
                            if (progressBar.getVisibility() == View.VISIBLE)
                                progressBar.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });

// Access the RequestQueue through your singleton class.
        AppController.getInstance(MainActivity.this).addToRequestQueue(jsObjRequest);
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub

        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView lw, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        // TODO Auto-generated method stub
        this.currentVisibleItemCount = visibleItemCount;

    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {

            //loading and showing more data on listview bottom scroll-----------------------------
            if (listView.getLastVisiblePosition() == listView.getAdapter().getCount() - 1
                    && listView.getChildAt(listView.getChildCount() - 1).getBottom() <= listView.getHeight()) {
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());


                if (cd.isConnectingToInternet() == true) {
                    progressBar.setVisibility(View.VISIBLE);
                    getJsonData(url + "?after=" + toMore);
                } else {
                    Toast.makeText(MainActivity.this, "No Internet connection", Toast.LENGTH_SHORT).show();
                }


            }


        }
    }
}

