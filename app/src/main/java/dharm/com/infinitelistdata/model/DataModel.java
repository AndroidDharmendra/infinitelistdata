package dharm.com.infinitelistdata.model;

public class DataModel {
	private String title;
	private String thumbnailUrl;
	private String largeImage_url;
	private String permalinkstr;
	public String getPermalinkstr() {
		return permalinkstr;
	}

	public void setPermalinkstr(String permalinkstr) {
		this.permalinkstr = permalinkstr;
	}




	public DataModel() {
	}



	public String getTitle() {
		return title;
	}

	public void setTitle(String name) {
		this.title = name;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getLargeImage_url(){return largeImage_url;}

	public void setLargeImage_url(String largeImage_url){this.largeImage_url=largeImage_url;}



}
