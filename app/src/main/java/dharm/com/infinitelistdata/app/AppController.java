package dharm.com.infinitelistdata.app;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import dharm.com.infinitelistdata.util.MyLruBitmapCache;

/**
 * copied from the official documentation
 */
public class AppController {
 
    private static AppController mVolleyInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mContext;
 
    private AppController(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
        mImageLoader = new ImageLoader(mRequestQueue,
                new MyLruBitmapCache(MyLruBitmapCache.getCacheSize(context))
        );
    }
 
    public static synchronized AppController getInstance(Context context) {
        if (mVolleyInstance == null) {
            mVolleyInstance = new AppController(context);
        }
        return mVolleyInstance;
    }
 
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // use the application context
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }
 
    public <t> void addToRequestQueue(Request<t> req) {
        getRequestQueue().add(req);
    }
 
    public ImageLoader getImageLoader() {
        return mImageLoader;
    }
}
 