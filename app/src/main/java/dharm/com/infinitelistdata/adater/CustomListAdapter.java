package dharm.com.infinitelistdata.adater;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import dharm.com.infinitelistdata.R;
import dharm.com.infinitelistdata.WebViewActivity;
import dharm.com.infinitelistdata.app.AppController;
import dharm.com.infinitelistdata.model.DataModel;
import dharm.com.infinitelistdata.util.ConnectionDetector;


public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataModel> datalist;
    private ConnectionDetector cd;
    private ImageLoader imageLoader;
    DataModel m;

    public CustomListAdapter(Activity activity, List<DataModel> datalist) {
        this.activity = activity;
        this.datalist = datalist;
        imageLoader = AppController.getInstance(activity).getImageLoader();
        cd = new ConnectionDetector(activity);
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int location) {
        return datalist.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance(activity).getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        m = datalist.get(position);
        thumbNail.setTag(new Integer(position));
        title.setTag(new Integer(position));
        thumbNail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (cd.isConnectingToInternet() == true) {
                    Intent in = new Intent(activity, WebViewActivity.class);
                    in.putExtra("url", datalist.get(Integer.parseInt(v.getTag().toString())).getLargeImage_url());
                    in.putExtra("clickon", "");
                    activity.startActivity(in);
                } else {
                    Toast.makeText(activity, "No Internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet() == true) {
                    Intent in = new Intent(activity, WebViewActivity.class);
                    in.putExtra("clickon", "title");
                    in.putExtra("url", "https://www.reddit.com" + datalist.get(Integer.parseInt(v.getTag().toString())).getPermalinkstr());
                    activity.startActivity(in);
                } else {
                    Toast.makeText(activity, "No Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // getting movie data for the row


        // thumbnail image
        thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);

        // title
        title.setText(m.getTitle());
        return convertView;
    }

}